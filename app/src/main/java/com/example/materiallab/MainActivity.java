package com.example.materiallab;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;

public class MainActivity extends ActionBarActivity {
    public static final int NOTFICATION_ID = 225;
    private static final String PREF_NAME = "CLICK_COUNT";
    public static final String KEY_INT_COUNT = "KEY_INT_COUNT";

    private RadioGroup mIconSel;
    private EditText mTitle;
    private EditText mMessage;
    private CheckBox mAddActionButton;

    private NotificationCompat.Builder mNotifBulder;
    private NotificationManager mNotifMngr;
    private SharedPreferences mClickCountPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mIconSel = (RadioGroup) findViewById(R.id.rgIcons);
        mTitle = (EditText) findViewById(R.id.etTitle);
        mMessage = (EditText) findViewById(R.id.etMssage);
        mAddActionButton = (CheckBox) findViewById(R.id.cbAddActionButton);
        mNotifMngr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        mClickCountPref = getPreferences(PREF_NAME, MODE_PRIVATE);

    }


    public void onCreateNotification(View view){
      //add 1 to count if exists or start at 1
       int clickCount = mClickCountPref.getInt(KEY_INT_COUNT, 0);
       SharedPreferences.Editor editor = mClickCountPref.edit();
       editor.putInt(KEY_INT_COUNT, ++clickCount);
       editor.commit();
       mNotifBulder = new NotificationCompat.Builder(this);
       mNotifBulder.setContentTitle(mMessage.getText().toString());
       mNotifBulder.setContentText(mMessage.getText().toString());

       int selectd = mIconSel.getCheckedRadioButtonId();
       int iconRes;
       switch (selected){
           case R.id.rbAndroid:
               iconRes = R.drawable.ic_adb_white_24dp;
               break;
           case R.id.rbBluetooth:
               iconRes = R.drawable.ic_bluetooth_audio_white_24dp;
               break;
           case R.id.rbDiskFull:
               iconRes = R.drawable.ic_disc_full_white_24dp;
               break;
           default:
               iconRes = R.drawable.ic_drive_eta_white_24dp;
               return;

       }
      mNotifBulder.setSmallIcon(iconRes);
      if(mAddActionButton.isChecked()) {
          Intent cardIntent
          new Intent(this, CountActivity.class);
          cardIntent.putExtra(KEY_INT_COUNT, clickCount);
          PendingIntent cardPendingIntent =
                  PendingIntent.getActivity(
                          this,
                          0,
                          cardIntent,
                          PendingIntent.FLAG_UPDATE_CURRENT
                  );

          mNotifBulder.addAction(R.drawable.abc_ic_menu_share_mtrl_alpha, "Share",
                  cardPendingIntent);

      }
        Notification newNotif = mNotifBulder();























































