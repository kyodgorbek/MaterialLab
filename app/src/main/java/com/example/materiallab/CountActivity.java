package com.example.materiallab;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.AbsListView;

/**
 * Created by yodgorbek on 25.10.15.
 */
public class CountActivity extends ActionBarActivity {


    private RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_count);
        int count = getIntent().getIntExtra(MainActivity.KEY_INT_COUNT, 0);
        initRecyclerView(this, count);
    }

    private void initRecyclerView(Context context, int count) {
        mRecyclerView = (RecyclerView) findViewById(R.id.rv.DummyCards);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        mRecyclerView.setAdapter(new DummyCardAdapter(count));

    }
}


   }
}
